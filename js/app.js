// Problem no.1

var num1 = prompt("Please enter the first number");
var num2 = prompt("Please enter the second number");
var sum = Number(num1) + Number(num2);

console.log(sum);

document.write(Math.max(num1, num2));

// Problem no.2

var gba = prompt("Please enter your grade");

if (Number(gba) < 60) {
    document.write("F");
} else if (Number(gba) < 70) {
    document.write("D");
} else if (Number(gba) < 80) {
    document.write("C");
} else if (Number(gba) < 90) {
    document.write("B");
} else if (Number(gba) >= 90) {
    document.write("E");
}

// Problem no.3

var a = 5,
    b = 6,
    c = 7;

s = (a + b + c) / 2;

area = Math.sqrt(s * (s - a) * (s - b) * (s - c));

document.write(area);